/**
 * ChannelController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const recommendaEnigneApi = 'http://10.178.23.33:8000/rec/rec/'

var fs = require('fs');
var csv = require('csv-parse');
var inputChannelFile='assets/channels.csv';
var inputUserSubscriptionFile='assets/user_subscription.csv';
var axios = require("axios");

module.exports = {
  getUserAddOn: function(req,res) {
    let user_id = req.param('user_id');
    let usersSubscribed = UserSubscription.findOne({
      user_id:user_id
    }).then(function (user) {
      if(user === undefined){
        throw "User not found"
      }
      let userchannels = user.channellist.split(",");
      let subscribed= {}

      for(let channel_id = 1; channel_id<=80;channel_id++) {
        if(userchannels[channel_id - 1] > 0){
          subscribed[channel_id] = true
        }else {
          subscribed[channel_id] = false
        }
      }
      return subscribed;
    });

    let getChannels = Channel.find()

    let getPreferredGenres = axios.get(recommendaEnigneApi + user_id).then(function (movResponse) {
      let movies = movResponse.data
      let genreCount = {}
      let max_genre = 0
      let max_count = 0
      console.log(movResponse)
      for (let i = 0; i < (movies.length ? movies.length : 5); i++) {
        let movie = movies[i]
        let genres = movie['genre'].split('|')
        console.log(genres)
        genres.forEach(function (genre) {
          console.log(genre)
          console.log( genreCount[genre])
          genreCount[genre] = (genreCount[genre] || 0) + 1
          if (max_count < genreCount[genre]) {
            max_count = genreCount[genre]
            max_genre = genre
          }
        })
      }
      return {
         max_count: max_count,
        max_genre : max_genre
      }
    });

    Promise.all([usersSubscribed, getChannels, getPreferredGenres]).then(function(values) {
      let subscribed = values[0]
      let channels = values[1]
      let max_genre = values[2].max_genre


      let max_count = values[2].max_count
      let unscubscribedChannels = {}
      for (let index = 0; index < channels.length; index++) {
        let channel = channels[index]
        //   console.log(subscribed[channel.channel_id])
        if (!subscribed[channel.channel_id]) {
          unscubscribedChannels[channel.channel_id] = channel
        }
      }

      let responseChannels = []
      for(let channel_id in unscubscribedChannels ) {
        let channel = unscubscribedChannels[channel_id]
        if(channel.genres__name.toLowerCase() === max_genre.toLowerCase()) {
          responseChannels.push(channel)
        }
      }
      res.json(responseChannels)
    }).catch(function (err) {
      res.serverError(err)
    }).then(function () {
      console.log("DONE")
    });

  },


  createUserSubscriptionsDb: function(req,res) {
    var user_id = 0;
    fs.createReadStream(inputUserSubscriptionFile)
      .pipe(csv({ separator: ',' }))
      .on('data', (data) => {
        ++user_id
        var userSubs = {
          user_id: user_id
          , channellist: data.join(',')
        };
        UserSubscription.create(userSubs).exec(function (err, result) {
          if(err = undefined){
            console.log(err)
          }else{
            console.log(result)
          }
        });

        console.log(userSubs)})
      .on('end', () => {
        return res.ok();
      });
  },


  createChannelDb: function(req,res) {
    let index = 0;
    fs.createReadStream(inputChannelFile)
      .pipe(csv({ separator: ',' }))
      .on('data', (data) => {

        var channel = {
          channel_id: ++index,
          channel_number: data[0]
          , station_id: data[1]
          , title: data[2]
          ,is_adult: data[3]
          ,genres__id:data[4]
          , genres__name: data[5]
          , channel_logo: data[6]
        };
        Channel.create(channel).exec(function (err, result) {
          if(err = undefined){
            console.log(err)
          }else{
            console.log(result)
          }
        });

        console.log(channel)})
      .on('end', () => {
        return res.ok();
        // [
        //   { NAME: 'Daffy Duck', AGE: '24' },
        //   { NAME: 'Bugs Bunny', AGE: '22' }
        // ]
      });
  }

};

