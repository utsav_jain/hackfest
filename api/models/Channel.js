/**
 * Channel.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    channel_id: {
      type:'string'
    },
    channel_number: {
      type: 'string'
    },
    station_id: {
      type: 'string'
    },
    title: {
      type: 'string'
    },
    is_adult: {
      type:'string'
    },
    genres__id: {
      type: 'string'
    },
    genres__name: {
      type: 'string'
    },
    channel_logo: {
      type: 'string'
    }
  }

};

